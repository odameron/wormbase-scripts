#! /usr/bin/env bash

wbRoot="ftp://ftp.wormbase.org/pub/wormbase/releases/current-production-release/"
wbURL="${wbRoot}/ONTOLOGY/"

datasetDir="/home/olivier/ontology/wormbase"


### Retrieve latest version number
#wbVersion="276"
wbVersion=`curl --silent --list-only ${wbRoot} | grep letter | sed 's/letter\.WS//'`


##### ANNOTATIONS

### Retrieve annotations
wget --directory-prefix=${datasetDir} ${wbURL}phenotype_association.WS${wbVersion}.wb

### Convert annotations to RDF
python3 ~/projects/wormbase2rdf/phenotype2rdf.py phenotype_association.WS${wbVersion}.wb WS${wbVersion}


##### ONTOLOGY

### Retrieve ontology
wget --directory-prefix=${datasetDir} ${wbURL}phenotype_ontology.WS${wbVersion}.obo

### Convert ontology to RDF
robot convert --input phenotype_ontology.WS${wbVersion}.obo --output phenotype_ontology.WS${wbVersion}.owl

