# wormbase-scripts

Utility scripts for downloading files from wormbase (https://wormbase.org/) and converting them to RDF (depends on https://gitlab.com/odameron/wormbase2rdf).

# Todo

- [ ] replace `downloadData.bash` by a snakemake script

