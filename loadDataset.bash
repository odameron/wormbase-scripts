#! /usr/bin/env bash

wbVersion="276"

tdbDir="tdb-wormbase"
#tdbDir="tdb-apidJNK"

if [ -d ${tdbDir} ]; then
  rm -rf ${tdbDir}
fi

mkdir ${tdbDir}

${JENA_HOME}/bin/tdbloader2 --loc=${tdbDir} phenotype_association.WS${wbVersion}.ttl phenotype_ontology.WS${wbVersion}.owl ~/ontology/geneOntology/evidenceCode-20191208.owl
#${JENA_HOME}/bin/tdbloader2 --loc=${tdbDir}  mi.owl ~/projects/ubiquitin/E2E3/E2E3.ttl ~/projects/ubiquitin/locusName/locusNameUniprot.ttl
