#! /usr/bin/env bash

tdbDir="tdb-wormbase"
#tdbDir="tdb-apidJNK"

echo "REPOSITORY: ${tdbDir}"
echo ""

${FUSEKI_HOME}/fuseki-server --loc=/home/olivier/ontology/wormbase/${tdbDir}/ /wormbase
